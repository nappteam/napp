FROM java:8u45

#we are working based on a 'dist'...
ADD target/universal /app

RUN \
	apt-get install unzip && \
	cd /app && \
#... so we have to unzip it (==> should we prefer working on a 'activator stage')
	unzip napp-*.zip && \
	rm napp-*.zip && \
	mv napp-* napp

WORKDIR /app/napp

CMD rm -f RUNNING_PID && bin/napp

EXPOSE 9000
