nappControllers.controller('HomeCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.init = function() {
        $scope.loadPendingEvents();
        $scope.loadApprovedEvents();
        $scope.loadRefusedEvents();
    };

    $scope.loadPendingEvents = function() {
        $http.get('/me/events/pending').success(function(result){
            $scope.pendingEvents = result;
        });
    };

    $scope.loadApprovedEvents = function() {
        $http.get('/me/events/approved').success(function(result){
            $scope.approvedEvents = result;
        });
    };

    $scope.loadRefusedEvents = function() {
        $http.get('/me/events/refused').success(function(result){
            $scope.refusedEvents = result;
        });
    };

    $scope.changeStatus = function(eventId, newStatus){
        $http.put('/me/event/'+ eventId +'/changestatus', {'status': newStatus})
            .success(function(result){
               $scope.init();
            });

    };

}]);