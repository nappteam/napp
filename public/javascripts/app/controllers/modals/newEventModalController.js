nappControllers.controller('NewEventModalCtrl', ['$scope', '$modalInstance', '$http', '$filter',
    function($scope, $modalInstance, $http, $filter) {
    var self = this;

    this.loadUsers = function() {
        $http.get('/users').success(function(result){
            $scope.users = result;
        });
    };

    $scope.ok = function () {
        var tags = angular.element("#tagSelect").val().split(',');
        var data = {
            name: $scope.name,
            date: $filter('date')($scope.dt, 'yyyy-MM-dd HH:mm'),
            tags: $scope.tags ? $scope.tags : [],
            place: $scope.location,
            users: $scope.guests ? $scope.guests : []
        };

        $http.put('/event', data).success(function(res){
            alert('ok');
        });
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.format = 'dd/MM/yyyy';

    $scope.clear = function () {
        $scope.dt = null;
    };

    this.init = function(){
        this.loadUsers();
    };

    this.init();
}]);