nappControllers.controller('EventsCtrl', ['$scope', '$modal', '$http', function($scope, $modal,$http) {

    $scope.onNewEventClick = function () {
        var modalInstance = $modal.open({
            templateUrl: 'assets/partials/modals/events/newEventModal.html',
            controller: 'NewEventModalCtrl as newEvent',
            size: 'lg'
        });

        modalInstance.result.then(function () {
            $scope.loadEvents();
        }, function () {
            console.log('failed');
        });
    };

    $scope.loadEvents = function() {
        $http.get('/events').success(function(result){
            $scope.events = result;
        });
    };

     var init =  function() {
         $scope.loadEvents();

     };

    init();
}]);