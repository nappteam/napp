nappControllers.controller('MapCtrl', ['$scope','$http', function($scope,$http) {
   $scope.init = function() {
       var map = L.map('leafletmap');//.setView([49.505871, 6.014258], 13);
       L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
           attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
       }).addTo(map);

       $http.get('/users').success(function(users){
           var tab = [];
           angular.forEach(users,function(u){
              if (u.latitude != null && u.longitude != null){
                var newU = getCoordonate(tab,u);
                console.log("U" + u.latitude + ',' + u.longitude + " - newU " + newU.latitude + ',' + newU.longitude);
                var marker = L.marker([newU.latitude, newU.longitude]).addTo(map);
                marker.bindPopup("<b>"+ newU.firstName + " " + newU.lastName+"</b>");
                tab.push([newU.latitude, newU.longitude]) ;
              }
           });

           map.fitBounds(tab);
       });
   }

    /* Check if user exists at the same location, if true, move to show all people on the map */
    var getCoordonate = function(list, u){
        if(list != null && list.length > 0){
            var i = 0;
            var found = false;
            while(!found && i < list.length){
                if(list[i][0] == u.latitude && list[i][1] == u.longitude)
                    found = true
                else
                    i++;
            }
            if(found){
                u.latitude = u.latitude + 0.00005;
                u.longitude = u.longitude + 0.00005;
                return getCoordonate(list,u);
            }
        }

        return u;
    };

   $scope.init();
}]);