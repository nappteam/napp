var nappControllers = angular.module('napp.controllers', []);

nappControllers.controller('GlobalCtrl', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {

    $rootScope.$on('$locationChangeSuccess', function(event){
        $rootScope.currentPath = $location.path();
    });

}]);