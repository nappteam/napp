nappApp.directive('select2Tags', function(){
    function link(scope, element, attrs, ngModel){
        ngModel.$render = function() {
            element.select2({tags: ngModel.$viewValue ? ngModel.$viewValue : []});
        };

        angular.element(element).bind("click", function (event)  {
            ngModel.$setViewValue(element.val().split(','));
                event.preventDefault();
        });
    }

   return {
       require: 'ngModel',
       link: link
   };
});