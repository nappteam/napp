var nappApp = angular.module('napp', ['ngRoute', 'napp.controllers', 'ui.bootstrap']);

nappApp.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/home', {
            templateUrl: '/assets/partials/home.html',
            controller: 'HomeCtrl'
        })
        .when('/events', {
            templateUrl: '/assets/partials/events.html',
            controller: 'EventsCtrl as events'
        })
        .when('/map', {
            templateUrl: '/assets/partials/map.html',
            controller: 'MapCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
}]);