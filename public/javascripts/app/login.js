angular.module('login', [])
    .controller('LoginCtrl', function($scope){
        $scope.getPositionAndLogin = function(){

            var submit = function(){
                $('#loginForm').submit();
            };

            if (navigator.geolocation)
                navigator.geolocation.getCurrentPosition(function(pos){
                    var latitude = pos.coords.latitude;
                    var longitude = pos.coords.longitude;
                    $('#latitude').val(latitude);
                    $('#longitude').val(longitude);
                    submit();
                }, function(err){
                    submit();
                });
            else
                submit();
        };
    });