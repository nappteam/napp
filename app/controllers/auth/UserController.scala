package controllers.auth

import controllers.SecuredController
import models.UserDAO
import play.api.libs.json.{Json, JsArray}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by mathieu_canzerini on 30/09/2014.
 */
object UserController extends SecuredController{
  def getAllUsers = Authenticated.async{

    UserDAO.getAllUsers.map(list =>
      list.map(UserDAO.toJson)
    ).map(list=>Ok(Json.toJson(list)))
  }
}
