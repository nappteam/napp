package controllers.auth

import play.api.mvc._
import play.api.mvc.Controller
import controllers.SecuredController
import play.api.Logger
import models.{User, EventDao, Event, UserDAO}
import scala.concurrent.Future
import play.api.libs.json.{Json, JsValue}

/**
 * User: antoine
 */
object Home extends SecuredController {

  private val logger = Logger("napp.controllers.auth.home")

  def index = Authenticated{request =>
    logger.debug(s"Show index page of user: ${request.user.login}")
    Ok(views.html.pages.auth.home(request.user))
  }

  def logout = Authenticated{request =>
    logger.debug(s"User ${request.user.login} log out.")
    // Redirige vers la page d'index en supprimant le cookie de session
    Redirect(controllers.unauth.routes.Main.index()).withNewSession
  }



  def getUsers = ???

}
