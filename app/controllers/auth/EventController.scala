package controllers.auth

import controllers.auth.UserController._
import play.api.libs.json.{JsArray, Json, JsValue}
import models.{UserDAO, Event, EventDao, Participant}
import scala.concurrent.Future
import controllers.SecuredController
import scala.concurrent.ExecutionContext.Implicits.global


object EventController extends SecuredController{
  def createEvent = Authenticated.async{request =>
    val jsonBody: Option[JsValue] = request.body.asJson

    // Expecting text body
    jsonBody.map {
      jsonBody => {
        val name = jsonBody.\("name").as[String]
        val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm")
        val date = format.parse(jsonBody.\("date").as[String])
        val place = jsonBody.\("place").as[String]
        val userIds:List[Participant] = jsonBody.\("users").as [JsArray].value.map(Participant.participantFromNewEventJson).toList
        val tags:List[String] = jsonBody.\("tags").as[List[String]]
        EventDao.createEvent(name,place,date,tags,userIds).map {
          result: Option[Event] =>
            result match {
              case Some(event:Event) => Created
              case None => InternalServerError("event has not been created")
            }
        }
      }
    }.getOrElse {
      Future.successful(BadRequest)
    }

  }

  def getEvent(id:String) = Authenticated.async { request =>
    EventDao.getEvent(id).map({
      case Some(event /*:Event*/) => Ok({
        EventDao.toJson(event)
      })
      case None => NotFound("")
    })
  }

  def getAllEvents() = Authenticated.async{request =>
    EventDao.getAllEvents().map(list =>
      list.map(EventDao.toJson)
    ).map(list=>Ok(Json.toJson(list)))
  }

  def getAllMyEventsByStatus(status:String) = Authenticated.async{request =>
    EventDao.getAllMyEventsByStatus(request.user.login,status).map(list =>
      list.map(EventDao.toJson)
    ).map(list=>Ok(Json.toJson(list)))
  }

  def changeStatus(id:String) = Authenticated.async{request =>
    val jsonBody: Option[JsValue] = request.body.asJson
    // Expecting text body
    jsonBody.map {
      jsonBody => {
        val status = jsonBody.\("status").as[String]
        EventDao.updateParticipationEvent(request.user.login,id,status).map {
          result: Option[String] =>
            result match {
              case Some("OK") => Ok
              case None => InternalServerError("event has not been updated")
            }
        }
      }
    }.getOrElse {
      Future.successful(BadRequest)
    }

  }

}
