package controllers

import play.api.mvc._
import models.{UserDAO, User}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Logger

/**
  * User: antoine
 */
class SecuredController extends Controller {

  private val logger = Logger("napp.controllers.secured")

  class UserRequest[A](val user:User, request: Request[A]) extends WrappedRequest[A](request)

  object Authenticated extends ActionBuilder[UserRequest] {
    def invokeBlock[A](request: Request[A], block: (UserRequest[A]) => Future[Result]) = {
      request.session.get("login").map {
        login =>
          UserDAO.findByLogin(login).flatMap {
            case Some(user) => block(new UserRequest[A](user, request))
            case _ =>
              logger.error(s"User $login found in session but doesn't exists in database")
              Future.successful(Forbidden)
          }
      }.getOrElse {
        logger.error("Unauthenticated access to protected resource")
        Future.successful(Forbidden)
      }
    }
  }


}
