package controllers.unauth

import play.api.mvc._
import models.UserDAO
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import play.api.Logger

object Main extends Controller {

  private val logger = Logger("napp.controllers.unauth.main")

  def index = Action {
    Ok(views.html.pages.unauth.index())
  }

  def register = Action {
    Ok(views.html.pages.unauth.register())
  }

  def login = Action {
    Ok(views.html.pages.unauth.login())
  }

  def createUser = Action.async(parse.urlFormEncoded) { request=>
    logger.debug("Create new user...")
    // Récupère les valeurs postées
    val inFirstName = getFormValue(request.body, "firstName")
    val inLastName  = getFormValue(request.body, "lastName")
    val inLogin     = getFormValue(request.body, "login")
    val inPassword  = getFormValue(request.body, "password")
    // Vérifie la validité des données postées
    (inFirstName, inLastName, inLogin, inPassword) match {

      case (Some(firstName), Some(lastName), Some(login), Some(password)) =>
        logger.debug(s"Valid request for user $login")
        // Vérifie si le login n'existe pas
        UserDAO.existsLogin(login).flatMap{
          case true =>
            logger.warn(s"User with login $login already exists")
            // Le login existe déjà : affiche la page d'enregistrement avec le message d'erreur
            Future.successful(
              BadRequest(views.html.pages.unauth.register(Some("Cet identifiant existe déjà"),inFirstName, inLastName, inLogin))
            )
          case false =>
            UserDAO.createUser(login, firstName, lastName, password).map{
              case false =>
                logger.error(s"Error during creation of user $login")
                // Une erreur est survenue à la création
                InternalServerError(views.html.pages.unauth.register(Some("Une erreur est survenue durant la création de votre compte"),inFirstName, inLastName, inLogin))
              case true =>
                // L'utilisateur a été créé !
                // Redirection vers la page d'accueil avec un cookie de session
                logger.info(s"User $login successfully created")
                Redirect(controllers.auth.routes.Home.index()).withSession("login" -> login)
            }
        }

      case _ =>
        logger.warn("Bad request for createUser")
        // Certains champs manquent
        Future.successful(
          BadRequest(views.html.pages.unauth.register(Some("Tous les champs doivent être renseignés"),inFirstName, inLastName, inLogin))
        )
    }
  }

  def doLogin = Action.async(parse.urlFormEncoded){
    request =>
      // Récupère les paramètres postés
      val inLogin = getFormValue(request.body, "login")
      val inPassword = getFormValue(request.body, "password")
      val inLat = getFormValue(request.body, "latitude")
      val inLong = getFormValue(request.body, "longitude")
      logger.debug(s"Login request for user $inLogin")
      (inLogin,inPassword,inLat,inLong) match {
        case (Some(login),Some(password),latitude,longitude) =>

          val doubleLatitude = latitude.map(_.toDouble)
          val doubleLongitude = longitude.map(_.toDouble)

          // Les paramètres sont présents, cherche l'utilisateur en base
          UserDAO.authenticate(login,password,doubleLatitude,doubleLongitude).map{
            case Some(user) =>
              // L'utilisateur existe: redirection vers la page d'accueil avec un cookie de session
              logger.info(s"Successful login for user $login")
              Redirect(controllers.auth.routes.Home.index()).withSession("login" -> login)
            case _ =>
              // L'utilisateur n'existe pas : affiche un message d'erreur
              logger.warn(s"Invalid login for user $login")
              Forbidden(views.html.pages.unauth.login(Some("Identifiant ou mot de passe incorrect")))
          }
        case _ =>
          // Les paramètres n'ont pas été envoyés : affiche un message d'erreur
          logger.warn(s"Bad login request")
          Future.successful(
            BadRequest(views.html.pages.unauth.login(Some("Paramètres manquants dans le formulaire")))
          )
      }
  }

  private def getFormValue(form:Map[String,Seq[String]], field:String):Option[String] =
    form.get(field) match {
      case Some(value +: _) if value.trim.length > 0 => Some(value.trim)
      case _                => None
    }

}