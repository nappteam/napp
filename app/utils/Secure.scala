package utils

import java.security.MessageDigest
import org.apache.commons.codec.binary.Base64

/**
 * User: antoine
 * Date: 27/06/2014 22:48
 */
object Secure {

  private val digester = MessageDigest.getInstance("SHA-1")

  def hashPassword(pass:String):String = Base64.encodeBase64String(digester.digest(pass.getBytes))

}
