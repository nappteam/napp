package models

import java.util.Date

import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson.BSONObjectID
import reactivemongo.core.commands.LastError

import scala.concurrent.Future
import play.api.Play.current
import play.api.Logger

import play.api.libs.json.{JsArray, JsObject, Json, JsValue}
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json.collection.JSONCollection

case class Event(id: Option[String],name:String, place:String, date: Date, tags: List[String],participants: List[Participant])

case class Participant(id: String, status: String)

object Participant{
  val STATUS_PENDING = "pending"
  val STATUS_APPROVED = "approved"
  val STATUS_REFUSED = "refused"

  def participantFromJson(js:JsValue):Participant = {
    val id = (js \ "id").as [String]
    val status = (js \ "status").as [String]
    Participant(id,status)
  }

  def participantToJson(participant:Participant): JsValue = Json.obj(
    "_id" -> participant.id,
    "status" -> participant.status
  )

  def fromMongoDocument(js:JsValue):Participant =
    Participant(
      id            = (js \ "_id").as[String],
      status          = (js \ "status").as[String]
  )

  def toMongoDocument(participant:Participant): JsValue = Json.obj(
    "_id" -> participant.id,
    "status" -> participant.status
  )

  def participantFromNewEventJson(js:JsValue): Participant = {
    val id = (js).as [String]
    val status = STATUS_PENDING
    Participant(id,status)
  }
}

object EventDao{

  val accessLogger: Logger = Logger("napp.eventDao")
  
  // Connection MongoDB
  private val db = ReactiveMongoPlugin.db
  // Collection des Events
  private val collection = db.collection[JSONCollection]("events")

  //Crée un objet JSON représentant un objet Event
  def toJson(event:Event):JsValue = Json.obj(
    "_id" -> event.id,
    "name" -> event.name,
    "date" -> event.date,
    "place" -> event.place,
    "tags" -> event.tags,
    "participants"  -> event.participants.map(Participant.participantToJson)
  )

   def fromMongoDocument(js:JsValue):Event =
    Event(
      id            = Some((js \ "_id").as[String]),
      name          = (js \ "name").as[String],
      place         = (js \ "place").as[String],
      date          = (js \ "date").as[Date],
      tags          = (js \ "tags").as[List[String]],
      participants  = (js \ "participants").as [JsArray].value.map(Participant.fromMongoDocument).toList
    )


  def updateParticipationEvent(login:String, idevent:String, statusToApply:String):Future[Option[String]] = {
    accessLogger.debug(login+":"+idevent+":"+statusToApply)
    collection.update(
      selector = Json.obj("_id" -> idevent, "participants._id" -> login),
      update = Json.obj("$set" -> Json.obj("participants.$.status" -> statusToApply))
    ).map({ last: LastError =>
      if(last.err.isEmpty)
        Some("OK")
      else None
    })
  }

  def createEvent(name:String, place:String, date: Date, tags: List[String],participants: List[Participant]):Future[Option[Event]] = {
    val id = BSONObjectID.generate.stringify
    collection.insert(
      Json.obj(
        "_id" -> id,
        "name" -> name,
        "place" -> place,
        "date" -> date,
        "tags" -> tags,
        "participants"  -> participants.map(Participant.toMongoDocument)
      )
    ).map({ last: LastError =>
        if(last.err.isEmpty)
          Some(Event(Some(id),name,place,date,tags,participants))
        else None
    })
  }

  def getEvent(id:String):Future[Option[Event]] = {
    collection.find(Json.obj("_id" -> id)).one[JsValue].map{
      case Some(js) => Some(fromMongoDocument(js))
      case _        => None
    }
  }

  def getAllEvents():Future[List[Event]] = {
    collection.find(JsObject(Seq())).cursor[JsValue].collect[List]().map{list=>
      list.map(fromMongoDocument)
    }
  }

  def getAllMyEventsByStatus(id:String,status:String):Future[List[Event]] = {
    collection.find(Json.obj("participants" -> Json.obj("$elemMatch" ->  Json.obj(
      "_id" -> id,
      "status" -> status
    )))  ).cursor[JsValue].collect[List]().map{list=>
      list.map(fromMongoDocument)
    }
  }
}

