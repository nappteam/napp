package models

import scala.concurrent.Future
import play.modules.reactivemongo.ReactiveMongoPlugin
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.Play.current
import reactivemongo.api._
import play.api.libs.json._
import utils.Secure
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson.{BSONDateTime, BSONObjectID, BSONDocument}
import play.modules.reactivemongo.json.BSONFormats._
import java.util.Date
import org.joda.time.format.ISODateTimeFormat

/**
 * User: antoine
 */

case class User(login:String, firstName:String, lastName: String,latitude:Option[Double],longitude:Option[Double], lastPositionDate:Option[Date])

object UserDAO{

  // Connection MongoDB
  private val db = ReactiveMongoPlugin.db
  // Collection des Users
  private val collection = db.collection[JSONCollection]("users")

  //Crée un objet JSON représentant un objet User
  def toJson(user:User):JsValue = Json.obj(
    "login" -> user.login,
    "firstName" -> user.firstName,
    "lastName" -> user.lastName,
    "latitude" -> user.latitude,
    "longitude" -> user.longitude,
    "lastPositionDate" -> user.lastPositionDate.map(_.getTime)

  )

  // Crée une instance de User à partir d'un document MongoDB
  private def fromMongoDocument(js:JsValue):User =
    User(
      login     = (js \ "_id").as[String],
      firstName = (js \ "firstName").as[String],
      lastName  = (js \ "lastName").as[String],
      latitude = (js \ "latitude").asOpt[Double],
      longitude = (js \ "longitude").asOpt[Double],
      lastPositionDate = (js \ "lastPositionDate").asOpt[Date]
    )

  // Vérifie si un login existe déjà dans la base
  def existsLogin(login:String):Future[Boolean] =
    collection.find(
      Json.obj("_id" -> login)
    ).one[JsValue].map(_.isDefined)

  // Crée un nouvel utilisateur
  def createUser(login:String, firstName:String, lastName:String, password:String):Future[Boolean] =
    collection.insert(
      Json.obj(
        "_id"           -> login,
        "firstName"     -> firstName,
        "lastName"      -> lastName,
        "password"      -> Secure.hashPassword(password),
        "creationDate"  -> Json.obj("$date" -> System.currentTimeMillis())
      )
    ).map(_.err.isEmpty)

  //Modifier la position d'un utilisateur
  def updatePositionUser(login:String,latitude:Double,longitude:Double) = {
    val modifier = BSONDocument(
      // this modifier will set the fields 'updateDate', 'title', 'content', and 'publisher'
      "$set" -> BSONDocument(
        "latitude" -> Some(latitude),
        "longitude" -> Some(longitude),
        "lastPositionDate" -> BSONDateTime(System.currentTimeMillis())
      )
    )
    // ok, let's do the update
    collection.update(BSONDocument("_id" -> login), modifier)
  }

  // Récupère un utilisateur à partir d'un login/password
  def authenticate(login:String, password:String,latitude:Option[Double],longitude:Option[Double]):Future[Option[User]] =
    collection.find(
      Json.obj(
        "_id"       -> login,
        "password"  -> Secure.hashPassword(password)
      )
    ).one[JsValue].map{
      case Some(js) =>
        (latitude,longitude) match {
          case (Some(lat),Some(long)) => updatePositionUser(login,lat,long)
          case _ => 
        }
        Some(fromMongoDocument(js))
      case _        => None
    }

  // Récupère un utilisateur à partir d'un login
  def findByLogin(login:String):Future[Option[User]] =
    collection.find(
      Json.obj(
        "_id"       -> login
      )
    ).one[JsValue].map{
      case Some(js) => Some(fromMongoDocument(js))
      case _        => None
    }

  //Récupère tous les utilisateurs de la base
  def getAllUsers:Future[List[User]] =
    collection.find(JsObject(Seq())).cursor[JsValue].collect[List]().map{list=>
      list.map(fromMongoDocument)
    }

}
