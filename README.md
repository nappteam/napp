N'App
=====

Localisez vos collègues pour une bonne bouffe.

- *C'est N'App porte quoi !*, **un inconnu**
- *Je ne me sens plus jamais seul*, **Florian**
- *Mais ça ressemble à chaudasse, non ?*, **Seb**
- *Il faut être peu pour bien dîner*, **un nain connu**
- *Quand il y en napp.lu, y en a encore*, **Florian**


//TODO: Evolution future pour fusionner à l'idée de ChaudasseFinder

